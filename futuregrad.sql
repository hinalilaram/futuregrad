-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2021 at 04:58 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `image`, `title`, `description`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'carousel-1.png', 'First slide label', 'Some representative placeholder content for the first slide.', 1, NULL, NULL, NULL),
(2, 'carousel-2.png', 'Second slide label', 'Some representative placeholder content for the first slide.', 1, NULL, NULL, NULL),
(3, 'carousel-3.png', 'Thrird slide label', 'Some representative placeholder content for the first slide.', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(55) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `image`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Chemistry', 'Want to kickstart your career in Chemistry? We got top colleges for you to apply.', 'chemistry.png', 1, NULL, NULL, NULL),
(2, 'Medical', 'Want to kickstart your career in Medical? We got top colleges for you to apply.', 'medical.png', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `colleges`
--

CREATE TABLE `colleges` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `name` varchar(55) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `type` varchar(55) DEFAULT NULL,
  `level` varchar(55) DEFAULT NULL,
  `campus_setting` varchar(55) DEFAULT NULL,
  `SAT` varchar(11) DEFAULT 'YES',
  `ACT` varchar(11) DEFAULT 'YES',
  `transcript` varchar(11) DEFAULT 'YES',
  `application_type` varchar(55) DEFAULT NULL,
  `deadline` varchar(55) DEFAULT NULL,
  `reply_deadline` varchar(55) DEFAULT NULL,
  `in_state_std` int(11) DEFAULT NULL,
  `out_of_state_std` int(11) DEFAULT NULL,
  `higgest_degree` varchar(55) DEFAULT NULL,
  `total_std` int(11) NOT NULL,
  `total_ugrad` int(11) DEFAULT NULL,
  `avg_net_tuition` int(11) DEFAULT NULL,
  `in_state_tuition` int(11) DEFAULT NULL,
  `out_of_state_tuition` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `colleges`
--

INSERT INTO `colleges` (`id`, `cat_id`, `name`, `image`, `icon`, `description`, `address`, `type`, `level`, `campus_setting`, `SAT`, `ACT`, `transcript`, `application_type`, `deadline`, `reply_deadline`, `in_state_std`, `out_of_state_std`, `higgest_degree`, `total_std`, `total_ugrad`, `avg_net_tuition`, `in_state_tuition`, `out_of_state_tuition`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Harvard University', 'harvard.png', NULL, 'Harvard University is a private Ivy League research university in Cambridge, Massachusetts. Founded in 1636 as Harvard College and named for its first benefactor, Puritan clergyman John Harvard, it is the oldest institution of higher learning in the United States and among the most prestigious in the world.', 'Cambridge, MA', 'Public', 'Public', 'Major City', 'YES', 'YES', 'YES', 'Fall Regular Decision', 'November 30, 2020 ', NULL, 88, 13, 'Doctorate', 44537, 31577, 15718, 13239, 29754, 1, NULL, NULL, NULL),
(2, 2, 'Stanford College', 'stanford.png', NULL, 'Harvard University is a private Ivy League research university in Cambridge, Massachusetts. Founded in 1636 as Harvard College and named for its first benefactor, Puritan clergyman John Harvard, it is the oldest institution of higher learning in the United States and among the most prestigious in the world.', 'Cambridge, MA', 'Private', 'Public', 'Major City', 'YES', 'YES', 'YES', 'Fall Regular Decision', 'November 30, 2020 ', NULL, 88, 13, 'Doctorate', 44537, 31577, 15718, 13239, 29754, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `features`
--

CREATE TABLE `features` (
  `id` int(11) NOT NULL,
  `name` varchar(55) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `features`
--

INSERT INTO `features` (`id`, `name`, `description`, `image`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admission Details', 'Are you worried about the admission schedule? Not anymore. Because Find your dream college provides you with the admission requirements, documents and dates that you should know.', 'business-card.png', 1, NULL, NULL, NULL),
(2, 'College Information', 'fururegrad provides you with the detailed information page of the colleges that best suits your skills.', 'archive.png', 1, NULL, NULL, NULL),
(3, 'Filters', 'Finding a college that matches you can be hard some times. With fururegrad, that is made much easier with the ability to filter colleges by state, skill and gender.', 'search.png', 1, NULL, NULL, NULL),
(4, 'Android Application', 'fururegrad goes everywhere you go with the fururegrad android application. The android app serves the same purpose as the web app, so you don\'t miss on any update and feature.', 'android.png', 1, NULL, NULL, NULL),
(5, 'Interactive Map', 'To make finding a college more visual, Find your dream college offers an interactive map, which allows users to move and zoom around a map with all the colleges displayed by pins at their locations.', 'map-.png', 1, NULL, NULL, NULL),
(6, 'Constant Updates', 'The Find your best college behind the scenes software is smart enough to know the current rankings, records and admission dates of the college. The staff are constantly active and in links with the colleges to get the current updates from them.', 'exchange.png', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `description`, `author`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'MY FIRST POST', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Hina Lilaram', '2021-07-28 19:00:00', '2021-07-28 19:00:00', NULL),
(2, 'MY SECOND POST', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Hina Lilaram', '2021-07-28 19:00:00', '2021-07-28 19:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `queries`
--

CREATE TABLE `queries` (
  `id` int(11) NOT NULL,
  `email` varchar(55) DEFAULT NULL,
  `name` varchar(55) DEFAULT NULL,
  `query` varchar(1000) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `queries`
--

INSERT INTO `queries` (`id`, `email`, `name`, `query`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'asd@add.com', 'asd', 'asdasd', '2021-09-14 05:21:23', '2021-09-14 05:21:23', NULL),
(2, 'asd@add.com', 'asd', 'asdasd', '2021-09-14 05:21:35', '2021-09-14 05:21:35', NULL),
(3, 'test@test.com', 'set', 'set', '2021-09-14 07:56:16', '2021-09-14 07:56:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(55) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `review` varchar(255) DEFAULT NULL,
  `rating` varchar(55) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `name`, `title`, `image`, `review`, `rating`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Patrick Muriungi', 'CEO & founder', 'profile.png', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Consequatur veritatis quia laboriosam blanditiis a consequuntur repudiandae rerum perferendis perspiciatis consectetur atque provident, dolore autem dolorem, iste, maxime corporis dignissimos accus', '5', 1, NULL, NULL, NULL),
(2, 'Joy Marete', 'Finance Manager', 'profile%2002.png', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Consequatur veritatis quia laboriosam blanditiis a consequuntur repudiandae rerum perferendis perspiciatis consectetur atque provident, dolore autem dolorem, iste, maxime corporis dignissimos accus', '4', 1, NULL, NULL, NULL),
(3, 'ClaireBelle Zawadi', 'Global brand manager', 'profile%2003.png', 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Consequatur veritatis quia laboriosam blanditiis a consequuntur repudiandae rerum perferendis perspiciatis consectetur atque provident, dolore autem dolorem, iste, maxime corporis dignissimos accus', '5', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sitecontents`
--

CREATE TABLE `sitecontents` (
  `id` int(11) NOT NULL,
  `type` varchar(55) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` varchar(1000) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sitecontents`
--

INSERT INTO `sitecontents` (`id`, `type`, `title`, `text`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'AU', 'Find a College that fits you best', '<i>fururegrad</i> is an online searchable database of colleges for you.\r\n                    Over 370+ colleges are in the <i>fururegrad</i> database, complete with links to their social links, \r\n                    pages, admission and academic calendar dates, college information, average acceptance rate and net cost.', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(11) NOT NULL,
  `name` varchar(55) DEFAULT NULL,
  `title` varchar(55) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `title`, `description`, `image`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Hina Lilaram', 'Full Stack Developer', 'Full Stack Developer', 'hina.png', 1, NULL, NULL, NULL),
(2, 'Sarmad Talpur', 'Front-end Developer', 'Front-end Developer', 'sarmad.png', 1, NULL, NULL, NULL),
(3, 'Sharjeel Ali', 'Front-end Developer', 'Front-end Developer', 'sharjeel.png', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `facebook`, `instagram`, `linkedin`, `logo`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL, NULL, NULL, 'admin@mailinator.com', NULL, '$2y$10$SIOdsbV60/qsB79FQ3TXe.wOmRMErBJ.O4ZM1N9LmAZvdf0//8sgi', NULL, '2021-09-12 14:58:17', '2021-09-12 14:58:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `colleges`
--
ALTER TABLE `colleges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `features`
--
ALTER TABLE `features`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `queries`
--
ALTER TABLE `queries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sitecontents`
--
ALTER TABLE `sitecontents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `colleges`
--
ALTER TABLE `colleges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `features`
--
ALTER TABLE `features`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `queries`
--
ALTER TABLE `queries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sitecontents`
--
ALTER TABLE `sitecontents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
