<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::get('/index', function () {
//     return redirect('/');
// });
Route::get('/','CollegeController@home');
Route::get('/contact', function () {
    return view('contact');
});
Route::get('/find','CollegeController@index');

Route::get('/details','CollegeController@details');
Route::post('/addquery','QueryController@add')->name('addquery');
Route::get('/map', function () {
    return view('map');
});
// Route::get('/', 'HomeController@index');
Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/home', function () {
    return redirect('dashboard');});

//admin


Route::group(['middleware'=>['auth']],function(){

    Route::get('/dashboard', function () {
    return view('admin.dashboard');});
    Route::get('/icons', function () {
    return view('admin.icons');});
    Route::get('/college-detail', 'CollegeController@show');
    Route::get('/colleges', 'CollegeController@list');
    Route::get('/addcollege', 'CollegeController@addpage');
    Route::post('/add', 'CollegeController@add');
    Route::get('/delcollege', 'CollegeController@del');
    Route::get('/editcollege', 'CollegeController@editpage');
    Route::post('/edit', 'CollegeController@edit');\
    Route::get('/query', 'QueryController@query');
        
});
