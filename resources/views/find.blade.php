@extends("layouts.master")
@section("title")
Find College
@endsection

@section("content")
<?php
//dd($data);
?>
<style>
    #carouselExampleSlidesOnly .carousel-item:before {
    content: "";
    background-image: linear-gradient(
        to bottom,
        transparent,
        rgba(0, 0, 0, 0.8)
    );
    display: block;
    position: absolute;
    top: 0;
    width: 100vw;
    height: 100vh;
}

body {
    overflow-x: hidden;
}
</style>
 
     <!-- top image -->
     <div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="assets/carousel-1.png" class="d-block w-100" alt="top-image">
            <div class="carousel-caption p-3">
                <h2><u>Find a college that fits you best</u></h2>
                <p class="lead">"fururegrad" is an online, searchable database of colleges that offers the best college for you. Over 370+ colleges are
                    in the "fururegrad" database, complete with links to their recruitment forms, admissions and academics, plus college
                    information and tuition, average acceptance rate, and net cost.</p>
            </div>
          </div>
        </div>
      </div>
    <!-- database results -->
    <div class="row">

    <!-- filter starts -->
    <section class="p-3 col-md-3" style="background-color:#fff1fd;">
        <div class="container">

        <form class="pb-5" action="{{url('find')}}" action="GET" >
        <div class="form-group mt-5 mb-5">
            <h3>Filters</h3>
        </div>    

        <div class="form-group mb-3">
        <h6><strong>Search Collage by Name</strong> </h6>  
                    <input type="text" name="search" class="form-control rounded" placeholder="Search"  />
            </div>
            <div class="form-group mb-3">
            <h6><strong>Location</strong> </h6>  
                <select class="js-example-basic-single" class="form-control" style="width:100% !important" name="location">
                <option value="">Select Location</option>            
                <option value="Cambridge">Cambridge</option>
                        
                </select>    </div>
            <div class="form-group mb-3">
                    <div class=" row">
                        <div class="col-md-12">
                        <h6><strong>Price Range</strong> </h6>  
                        </div>
                        <div class="col-md-12">
                            
                        <label for="price-min">Min:</label>
                        <input type="range" name="price-min"oninput="$('#min').html(this.value)" id="price-min" value="0" min="0" max="500">
                        <span id="min">0</span>   
                    </div>
                        <div class="col-md-12">
                        <label for="price-max">Max:</label>
                            <input type="range"oninput="$('#max').html(this.value)" name="price-max" id="price-max" value="15000" min="15000" max="80000">
                            <span id="max">15000</span>
                        </div>
        
                    </div>
            </div>
                <div class="form-group">
                <h6><strong>Majors</strong> </h6>   
                <div class="form-check">
                
                    <input class="form-check-input" type="checkbox" value="1" name="majors[]" id="cs">
                    <label class="form-check-label" for="cs">
                    Computer Science
                    </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="2" name="majors[]" id="el">
                        <label class="form-check-label" for="el">
                        Electronics Engineering
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="1" name="majors[]" id="ar">
                        <label class="form-check-label" for="ar">
                    Architecture
                        </label>
                    </div>
                </div>
        <div class="form-group mt-3">
        <button type="submit" class="btn btn-sm btn-primary" style="color:white" >Search</button>
        <button type="reset" class="btn btn-sm btn-secondary" >Clear</button>
        </div>
        </form>

        </div>

    </section>

    <!-- filter ends -->


    <section id="contact" class="col-md-9">
        <div class="container-lg">
           
            <div class="row gy-4 justify-content-center align-items center">
@foreach($data as $row)
                <div class="col-10 col-lg-4">
                    <div class="card b-0 shadow my-4">
                        <img src="{{url('assets/'.$row->image)}}" class="card-img-top" alt="...">
                        <div class="card-body my-2">
                        <h5 class="card-title">{{$row->name}}</h5>
                        <p class="card-text text-muted">{{$row->address}}</p>
                        <a href="details?id={{$row->id}}" class="btn btn-primary text-white">See Details</a>
                        </div>
                    </div>
                </div>
@endforeach
                <!-- <div class="col-10 col-lg-4">
                    <div class="card b-0 shadow my-4">
                        <img src="assets/stanford.png" class="card-img-top" alt="...">
                        <div class="card-body my-2">
                            <h5 class="card-title">Stanford College</h5>
                            <p class="card-text text-muted">Stanford, CA</p>
                            <a href="#" class="btn btn-primary text-white">See Details</a>
                        </div>
                    </div>
                </div>

                <div class="col-10 col-lg-4">
                    <div class="card b-0 shadow my-4">
                        <img src="assets/florida.png" class="card-img-top" alt="...">
                        <div class="card-body my-2">
                            <h5 class="card-title">University of Florida</h5>
                            <p class="card-text text-muted">Gainesville, FL</p>
                            <a href="#" class="btn btn-primary text-white">See Details</a>
                        </div>
                    </div>
                </div>

                <div class="col-10 col-lg-4">
                    <div class="card b-0 shadow my-4">
                        <img src="assets/yale.png" class="card-img-top" alt="...">
                        <div class="card-body my-2">
                            <h5 class="card-title">Yale University</h5>
                            <p class="card-text text-muted">New Haven, CT</p>
                            <a href="#" class="btn btn-primary text-white">See Details</a>
                        </div>
                    </div>
                </div>

                <div class="col-10 col-lg-4">
                    <div class="card b-0 shadow my-4">
                        <img src="assets/chicago.png" class="card-img-top" alt="...">
                        <div class="card-body my-2">
                            <h5 class="card-title">The University of Chicago</h5>
                            <p class="card-text text-muted">Chicago, II.</p>
                            <a href="#" class="btn btn-primary text-white">See Details</a>
                        </div>
                    </div>
                </div>

                <div class="col-10 col-lg-4">
                    <div class="card b-0 shadow my-4">
                        <img src="assets/cornell.png" class="card-img-top" alt="...">
                        <div class="card-body my-2">
                            <h5 class="card-title">Cornell University</h5>
                            <p class="card-text text-muted">Ithaca, NY</p>
                            <a href="#" class="btn btn-primary text-white">See Details</a>
                        </div>
                    </div>
                </div> -->

            </div>
        </div>
      </section>
    
    </div>

    <script>
        $(document).ready(function() {
    $('.js-example-basic-single').select2();

  


});
    </script>
@endsection

      