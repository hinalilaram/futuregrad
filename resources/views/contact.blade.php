@extends("layouts.master")

@section("title")
Contact
@endsection

@section("content")

    <!-- form -->
    <!-- form-control, form-label, form-select, input-group, input-group-text -->
    <section id="contact">
        <div class="container-lg">

            <div class="text-center">
                <h2>Get in Touch</h2>
                <p class="lead">Questions to ask? Fill out the form to contact us...</p>
            </div>
            <div class="row justify-content-center my-5">
                <div class="col-lg-6">

                    <form action="javascript:;" method="post"  >
                        @csrf
                        <label for="email" class="form-label">Email address:</label>
                        <div class="input-group mb-4">
                            <span class="input-group-text">
                    <i class="bi bi-envelope-fill text-secondary"></i>
                  </span>
                            <input type="email" required id="email" name="email"class="form-control" placeholder="e.g. mario@example.com" />
                        </div>
                        <label for="name" class="form-label">Name:</label>
                        <div class="mb-4 input-group">
                            <span class="input-group-text">
                    <i class="bi bi-person-fill text-secondary"></i>
                  </span>
                            <input type="text" required id="name" name="name"class="form-control" placeholder="e.g. Mario" />
                        </div>
                        <div class="mb-4 mt-5 form-floating">
                            <textarea class="form-control" required id="query"name="query" style="height: 140px" placeholder="query"></textarea>
                            <label for="query">Your query...</label>
                        </div>
                        <div class="mb-4 text-center">
                            <button type="submit" class="btn btn-secondary">Submit</button>
                        </div>
                        <div class="alert alert-danger print-error-msg error" style="margin-top:5%; display:none">
                                          <ul></ul>
                                       </div>
                                       <div class="alert alert-success print-success-msg success" style="margin-top:5%;display:none">
                                       <ul></ul>
                                       </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

  

<script>
    $(document).ready(function(){

        function printMsg (msg,form) {
            if(form == "error"){
            $(".error").find("ul").html('');
            $('.error').fadeIn("fast", function(){        
            $(".error").delay(5000).fadeOut(4000);
            });
              $.each( msg, function( key, value ) {
                $(".error").find("ul").append('<li>'+value+'</li>');
            });}
            if(form == "success"){
            $(".success").find("ul").html('');
            $('.success').fadeIn("fast", function(){        
            $(".success").delay(5000).fadeOut(4000);
            $(".success").find("ul").append('<li>'+msg+'</li>');
            });
             }
        }
      $('form').submit(function(event) {
            event.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                url:"{{url('addquery')}}",
                method:"POST",
                data:formData,
                processData: false,
                contentType: false,
                success:function(data)
                { 
                if(data.error){ 
                printMsg(data.error,"error");
                }
                else{
                    printMsg(data.success,"success");
                }
                }
            });    });
    });
</script>

    @endsection