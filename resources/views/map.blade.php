@extends("layouts.master")
@section("title")
Map
@endsection

@section("content")

    <!-- map -->
    <section>
        <div class="container-lg">
            <div class="text-center">
                <h2>Interactive Map</h2>
                <p class="lead">Below is an interactive map so you visually find colleges that offer wrestling. By using the filters, you can filter out the colleges by their state, division and gender. The male colleges are signified by the red marker and the female by the blue marker. Clicking on a marker will bring up the college's information. Note that you can zoom in and pan around the map to find colleges.</p>
            </div>

            <div class="row justify-content-center my-5">
                <div class="col-10">
                    <div id="map" style="height: 500px;">

                    </div>
                </div>
            </div>
        </div>
    </section>


        <script>
            function initMap() {
                var location = {lat: 42.37557624252218, lng: -71.12191723189113};
                var map = new google.maps.Map(document.getElementById("map"), {
                    zoom: 4,
                    center: location
                });
            }
        </script>
@endsection