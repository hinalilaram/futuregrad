@extends("layouts.master")
@section("title")
College Details
@endsection

@section("content")
 <?php
 //dd($data);
 ?>
 
   @if($data != null)
    <!-- top image -->
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="{{url('assets/'.$data->image)}}" class="d-block w-100" alt="top-image">
          </div>
        </div>
      </div>

    <!-- college name and tag -->
    <div class="container my-5">
        <div class="row justify-content-center">
            <div class="col-11 col-md-12">
                <h1 class="display-5">{{$data->name}}</h1>
                <p class="text-muted lead">{{$data->address}}</p>
            </div>
            <div class="col-11 col-md-12">
                <p>{{$data->description}}</p>
            </div>
        </div>
          
        <!-- detailed info tabs -->
        <section>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist" aria-orientation="vertical">
                  <button class="nav-link active" id="admission-stats-tab" data-bs-toggle="tab" data-bs-target="#admission-stats" type="button" role="tab" aria-controls="admission-stats" aria-selected="true">Key Admission Stats</button>
                  <button class="nav-link" id="admission-requirements-tab" data-bs-toggle="tab" data-bs-target="#admission-requirements" type="button" role="tab" aria-controls="admission-requirements" aria-selected="false">Admission Requirements</button>
                  <button class="nav-link" id="important-deadlines-tab" data-bs-toggle="tab" data-bs-target="#important-deadlines" type="button" role="tab" aria-controls="important-deadlines" aria-selected="false">Important Deadlines</button>
                  <button class="nav-link" id="admitted-student-stats-tab" data-bs-toggle="tab" data-bs-target="#admitted-student-stats" type="button" role="tab" aria-controls="admitted-student-stats" aria-selected="false">Admitted Student Stats</button>
                  <button class="nav-link" id="academics-tab" data-bs-toggle="tab" data-bs-target="#academics" type="button" role="tab" aria-controls="academics" aria-selected="false">Academics</button>
                  <button class="nav-link" id="tuition-cost-tab" data-bs-toggle="tab" data-bs-target="#tuition-cost" type="button" role="tab" aria-controls="tuition-cost" aria-selected="false">Tuition Cost</button>
                </div>
              </nav>
              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="admission-stats" role="tabpanel" aria-labelledby="admission-stats-tab">
                    <div class="row justify-content-around my-5 bg-light p-4 rounded">
                        <div class="col-8 col-md-3">
                            <p>INSTITUTION TYPE</p>
                            <p class="lead fw-bold">{{$data->type}}</p>
                        </div>
                        <div class="col-8 col-md-3">
                            <p>LEVEL OF INSTITUTION</p>
                            <p class="lead fw-bold">{{$data->level}}</p>
                        </div>
                        <div class="col-8 col-md-3">
                            <p>CAMPUS SETTING</p>
                            <p class="lead fw-bold">{{$data->campus_setting}}</p>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="admission-requirements" role="tabpanel" aria-labelledby="admission-requirements-tab">
                    <div class="row justify-content-around my-5 bg-light p-4 rounded">
                        <div class="col-8 col-md-3">
                            <p>SAT</p>
                            <p class="lead fw-bold">{{$data->SAT}}</p>
                        </div>
                        <div class="col-8 col-md-3">
                            <p>ACT</p>
                            <p class="lead fw-bold">{{$data->ACT}}</p>
                        </div>
                        <div class="col-8 col-md-3">
                            <p>TRANSCRIPT</p>
                            <p class="lead fw-bold">{{$data->transcript}}</p>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="important-deadlines" role="tabpanel" aria-labelledby="important-deadlines-tab">
                    <div class="row justify-content-around my-5 bg-light p-4 rounded">
                        <div class="col-8 col-md-3">
                            <p>APPLICATION TYPE</p>
                            <p class="lead fw-bold">{{$data->application_type}}</p>
                        </div>
                        <div class="col-8 col-md-3">
                            <p>APPLICATION DEADLINE</p>
                            <p class="lead fw-bold">{{$data->deadline}}</p>
                        </div>
                        <div class="col-8 col-md-3">
                            <p>REPLY DEADLINE</p>
                            <p class="lead fw-bold">{{$data->reply_deadline}}</p>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="admitted-student-stats" role="tabpanel" aria-labelledby="admitted-student-stats-tab">
                    <div class="row justify-content-around my-5 bg-light p-4 rounded">
                        <div class="col-8 col-md-4">
                            <p>IN STATE STUDENTS</p>
                            <p class="lead fw-bold">{{$data->in_state_std}}%</p>
                        </div>
                        <div class="col-8 col-md-4">
                            <p>OUT OF STATE STUDENTS</p>
                            <p class="lead fw-bold">{{$data->out_of_state_std}}%</p>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="academics" role="tabpanel" aria-labelledby="academics-tab">
                    <div class="row justify-content-around my-5 bg-light p-4 rounded">
                        <div class="col-8 col-md-3">
                            <p>HIGHEST DEGREE OFFERED</p>
                            <p class="lead fw-bold">{{$data->highest_degree}}</p>
                        </div>
                        <div class="col-8 col-md-3">
                            <p>TOTAL NUMBER OF STUDENTS</p>
                            <p class="lead fw-bold">{{$data->total_std}}</p>
                        </div>
                        <div class="col-8 col-md-3">
                            <p>TOTAL NUMBER OF UNDERGRADS</p>
                            <p class="lead fw-bold">{{$data->total_ugrad}}</p>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="tuition-cost" role="tabpanel" aria-labelledby="tuition-cost-tab">
                    <div class="row justify-content-around my-5 bg-light p-4 rounded">
                        <div class="col-8 col-md-6 my-4">
                            <h3 class="mb-3">Key Financial Stats</h3>
                            <p class="lead">AVERAGE NET PRICE</p>
                            <h1 class="display-5">${{$data->avg_net_tuition}}</h1>
                        </div>
                        <div class="col-8 col-md-4">
                            <p>TUITION</p>
                            <hr>
                            <div class="row justify-content-around">
                                <div class="col-8">
                                    <p class="text-muted">In-State-Tuition</p>
                                </div>
                                <div class="col-4">
                                    <p class="text-muted">${{$data->in_state_tuition}}</p>
                                </div>
                            </div>
                            <hr>
                            <div class="row justify-content-around">
                                <div class="col-8">
                                    <p class="text-muted">Out-of-State-Tuition</p>
                                </div>
                                <div class="col-4">
                                    <p class="text-muted">${{$data->out_of_state_tuition}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

              </div>        
        </section>

    </div>
@else
 <h1 class="p-5" style="text-align:center">Oops.. Something Went Wrong</h1>
    @endif
    @endsection
