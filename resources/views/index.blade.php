@extends("layouts.master")
@section("title")
Home
@endsection

@section("content")
<?php
//dd($data);
?>
<style>
    #homeCarousel .carousel-item:before {
    content: "";
    background-image: linear-gradient(
        to bottom,
        transparent,
        rgba(0, 0, 0, 0.8)
    );
    display: block;
    position: absolute;
    top: 0;
    width: 100vw;
    height: 100vh;
}

</style>

    <!--  carousel -->
    <div id="homeCarousel" class="carousel slide mb-2" data-bs-ride="carousel">
        <div class="carousel-indicators">
        <?php $count = 1; ?>  
        @for($i = 0; $i < sizeof($data['banner']); $i++)
          
          <button type="button" data-bs-target="#homeCarousel" data-bs-slide-to="{{$i}}" class="<?php if($i==0){ ?>active<?php } ?>"  ></button>
          <?php $count++ ; ?>  
          @endfor
        </div>
        <div class="carousel-inner">
        @for( $i =0; $i < sizeof($data['banner']); $i++ )
          <div class="carousel-item  <?php if($i==0){ ?>active<?php } ?>">
              <img src="{{url('assets/'.$data['banner'][$i]['image'])}}" class="d-block w-100" alt="...">
              <div class="carousel-caption d-none d-md-block">
                <h3>{{$data['banner'][$i]['title']}}</h3>
                <p class="lead">{{$data['banner'][$i]['description']}}</p>
              </div>
            </div>
            @endfor
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#homeCarousel" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#homeCarousel" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>

      <!-- Tag -->
      <section>
        <div class="container-lg">
            <div class="row justify-content-center align-items-center">
                <div class="col-md-10 text-center">
                    <h1 class="display-5">{{$data['aboutus']->title}}</h1>
                    <p class="lead my-4 text-muted">  <?php echo html_entity_decode($data['aboutus']->text)?> </p>
            </div>
        </div>
      </section>

      <!-- features -->
      <section class="bg-light">
        <div class="container-lg">
            <div class="text-center">
                <h2>Features</h2>
            </div>

            <div class="row justify-content-center align-items center gy-4 mb-5">

              


              
@foreach($data['feature'] as $row)

                <div class="col-10 col-lg-4">
                  <div class="card b-0 shadow my-4 py-5">
                    <div class="card-body text-center">
                      <h4 class="card-title my-1"><img src="{{url('assets/'.$row->image)}}" alt="admission-icon" class="img-fluid" width="40px" height="40px">{{$row->title}}</h5>
                      <p class="card-text">{{$row->description}}</p>
                    </div>
                  </div>
                </div>
@endforeach
            </div>

        </div>
      </section>

      <!-- categories -->
      <section>
        <div class="container-lg">
            <div class="text-center">
                <h2>Categories</h2>
            </div>

            <div class="row gy-4 justify-content-center align-items center">
@foreach($data['cat'] as $row)
                <div class="col-10 col-lg-4">
                    <div class="card b-0 shadow my-4">
                        <img src="{{url('assets/'.$row->image)}}" class="card-img-top" alt="...">
                        <div class="card-body my-2">
                          <h5 class="card-title">{{$row->name}}</h5>
                          <p class="card-text">{{$row->description}}
                          </p>
                          <a href="find?cat={{$row->id}}" class="btn btn-primary text-white">Discover More</a>
                        </div>
                      </div>
                </div>
@endforeach
                <!-- <div class="col-10 col-lg-4">
                    <div class="card b-0 shadow my-4">
                        <img src="assets/medical.png" class="card-img-top" alt="...">
                        <div class="card-body my-2">
                          <h5 class="card-title">Medical</h5>
                          <p class="card-text">Want to kickstart your career in Medical?
                              We got top colleges for you to apply.
                          </p>
                          <a href="#" class="btn btn-primary text-white">Discover More</a>
                        </div>
                      </div>
                </div>

                <div class="col-10 col-lg-4">
                    <div class="card b-0 shadow my-4">
                        <img src="assets/computer science.png" class="card-img-top" alt="...">
                        <div class="card-body my-2">
                          <h5 class="card-title">Computer Science</h5>
                          <p class="card-text">Want to kickstart your career in Computer Science?
                              We got top colleges for you to apply.
                          </p>
                          <a href="#" class="btn btn-primary text-white">Discover More</a>
                        </div>
                      </div>
                </div>

                <div class="col-10 col-lg-4">
                    <div class="card b-0 shadow my-4">
                        <img src="assets/arts & design.png" class="card-img-top" alt="...">
                        <div class="card-body my-2">
                          <h5 class="card-title">Arts & Design</h5>
                          <p class="card-text">Want to kickstart your career in Arts & Design?
                              We got top colleges for you to apply.
                          </p>
                          <a href="#" class="btn btn-primary text-white">Discover More</a>
                        </div>
                      </div>
                </div>

                <div class="col-10 col-lg-4">
                    <div class="card b-0 shadow my-4">
                        <img src="assets/space.png" class="card-img-top" alt="...">
                        <div class="card-body my-2">
                          <h5 class="card-title">Space</h5>
                          <p class="card-text">Want to kickstart your career in Space?
                              We got top colleges for you to apply.
                          </p>
                          <a href="#" class="btn btn-primary text-white">Discover More</a>
                        </div>
                      </div>
                </div>

                <div class="col-10 col-lg-4">
                    <div class="card b-0 shadow my-4">
                        <img src="assets/engineering.png" class="card-img-top" alt="...">
                        <div class="card-body my-2">
                          <h5 class="card-title">Engineering</h5>
                          <p class="card-text">Want to kickstart your career in Engineering?
                              We got top colleges for you to apply.
                          </p>
                          <a href="#" class="btn btn-primary text-white">Discover More</a>
                        </div>
                      </div>
                </div> -->

            </div>

        </div>
      </section>

      <!-- Success Stories -->
      <section>
        <div class="container">
            <div class="row text-center mb-4">
                <h1>Succcess Stories</h1>
                <hr style="height: 3px; width: 200px;" class="mx-auto">
            </div>

            <div class="row align-items-center">
                <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                  <div class="carousel-inner bg-light">
                  @for( $i =0; $i < sizeof($data['review']); $i++ )
                    <!-- CAROUSEL ITEM 1 -->
                    <div class="carousel-item <?php if($i==0){ ?>active<?php } ?>">
                      <!-- story card  -->
                      <div>
                        <p class="lh-lg text-center p-3">
                          <i class="fas fa-quote-left"></i>
                          {{$data['review'][$i]['review']}}
                          <i class="fas fa-quote-right"></i>
                          <div class="ratings p-1 text-center">
                            @for($j = 1; $j <= $data['review'][$i]['rating'];  $j++)
                            <i class="bi bi-star-fill"></i>
                            @endfor
                          </div>
                        </p>
                      </div>
                      <!-- client picture  -->
                      <div class="text-center">
                        <img src="{{url('assets/'.$data['review'][$i]['image'])}}" width="150" height="150" alt="client-1 picture" class="rounded-circle img-fluid">
                      </div>
                      <!-- client name & role  -->
                      <div class="text-center">
                        <h3> {{$data['review'][$i]['name']}}</h3>
                        <p class="fw-light"> {{$data['review'][$i]['title']}}</p>
                      </div>
                    </div>     
                  @endfor
                
                    
                  </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="visually-hidden">Next</span>
                    </button>
                </div>
              </div>
            </div>
        </div>
      </section>

      <!-- counselors -->
      <section>
        <div class="container">
          <div class="row justify-content-center align-items-center text-center">
            <h1 class="display-5 text-center mb-5">Our Worthy Counselors</h1>
            @foreach($data['team'] as $row)
            <div class="col-6 col-lg-3 my-3">
              <img src="{{url('assets/'.$row->image)}}" alt="counselor picture" width="150px" height="150px" class="img-fluid rounded-circle">
              <h4 class="mt-3">{{$row->name}}</h4>
              <p class="lead">{{$row->title}}</p>
            </div>
            @endforeach
          </div>
        </div>
      </section>

    
@endsection