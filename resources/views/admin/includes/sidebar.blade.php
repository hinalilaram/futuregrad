<div  class="sidebar">
       
       <div class="sidebar-wrapper">
           <div class="logo">
               <a href="javascript:void(0)" class="simple-text logo-mini">
       <i class="fa fa-hat" ></i>
     </a>
               <a href="javascript:void(0)" class="simple-text logo-normal">
       futuregrad
     </a>
           </div>
           <ul class="nav">
               <li class="active ">
                   <a href="dashboard">
                       <i class="tim-icons icon-chart-pie-36"></i>
                       <p>Dashboard</p>
                   </a>
               </li>
               
               <!-- <li>
                   <a href="map">
                       <i class="tim-icons icon-pin"></i>
                       <p>Maps</p>
                   </a>
               </li> -->
               <!-- <li>
                   <a href="notifications">
                       <i class="tim-icons icon-bell-55"></i>
                       <p>Notifications</p>
                   </a>
               </li> -->
               <!-- <li>
                   <a href="user">
                       <i class="tim-icons icon-single-02"></i>
                       <p>Collage Details</p>
                   </a>
               </li> -->
               <li>
                   <a href="colleges">
                       <i class="tim-icons icon-bank"></i>
                       <p>Colleges</p>
                   </a>
               </li>
                <li>
                   <a href="query">
                       <i class="tim-icons icon-email-85"></i>
                       <p>Email Queries</p>
                   </a>
               </li>
               <!-- <li>
                   <a href="settings">
                       <i class="tim-icons icon-atom"></i>
                       <p>Site Settings</p>
                   </a>
               </li> -->
               <!-- <li>
                   <a href="typography">
                       <i class="tim-icons icon-align-center"></i>
                       <p>Typography</p>
                   </a>
               </li> -->
               <!-- <li>
                   <a href="rtl">
                       <i class="tim-icons icon-world"></i>
                       <p>RTL Support</p>
                   </a>
               </li> -->
               <!-- <li class="active-pro">
                   <a href="upgrade">
                       <i class="tim-icons icon-spaceship"></i>
                       <p>Upgrade to PRO</p>
                   </a>
               </li> -->
           </ul>
       </div>
   </div>