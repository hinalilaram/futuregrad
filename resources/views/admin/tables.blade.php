@extends('admin.layouts.master')
@section('title')

User
@endsection

@section('content')
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header">
                <h4 class="card-title">Collages</h4>
                <a href="{{url('addcollege')}}" class="float-right btn btn-primary"><i class="fa fa-plus mr-2"> </i> Add New College</a>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table tablesorter myTable " id="">
                    <thead class=" text-primary">
                      <tr>
                      <th>
                          Image
                        </th>
                        <th>
                          Name
                        </th>
                        <th>
                          Total Students
                        </th>
                        <th>
                          State
                        </th>
                        <th>
                          Type
                        </th>
                        <th>
                          Tuition
                        </th>
                        <th class="text-center">
                          Action
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($data as $row)
                      <tr>
                      <td>
                          <img width="100px" height="20%" src="{{url('assets/'.$row->image)}}" alt="">
                        </td>
                        <td>
                          {{$row->name}}
                        </td>
                        <td>
                          {{$row->total_std}}
                        </td>
                        <td>
                        {{$row->state}}
                        </td>
                        <td>
                        {{$row->type}}
                        </td>
                        <td>
                        ${{$row->avg_net_tuition}}
                        </td>
                        <td class="text-center">
                        <a type="button" rel="tooltip" href="{{url('editcollege?id='.$row->id)}}" title="Edit" class="btn btn-link" data-original-title="Edit Task">
                            <i class="tim-icons icon-pencil"></i>
                            </a>
                          <button type="button" rel="tooltip" title="Delete" data-attr="{{$row->id}}" class="del btn btn-link" data-original-title="Edit Task">
                            <i class="tim-icons icon-trash-simple"></i>
                          </button>
                        </td>
                      </tr>
                     @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        
        </div>
      </div>




<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> <i class="tim-icons icon-trash-simple"></i> Delete college</h5>

      </div>
      <div class="modal-body">
       Are you sure you want to delete this college?
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn yes btn-primary">Yes</button>
      </div>
    </div>
  </div>
</div>
      @endsection

@section('scripts')
      <script>
$(document).ready(function(){
  $('.del').click(function(){
      var id = $(this).attr('data-attr');
     // alert(id);

          $('#exampleModal').modal().show();

          $('.yes').click(function(){

                      $.get("delcollege?id="+id, function(data, status) {
                       if(data.success)
                        location.reload();
                      });
            });

      });

});
      </script>

@endsection