@extends('admin.layouts.master')
@section('title')
Email Queries
@endsection

@section('content')
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card ">
              <div class="card-header">
                <h4 class="card-title">Email Queries</h4>
             
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table tablesorter myTable" id="">
                    <thead class=" text-primary">
                      <tr>
                      <th>
                          Name
                        </th>
                        <th>
                          Email
                        </th>
                        <th>
                         Message
                        </th>
                     
                        <th>
                          Date Time
                        </th>
                       
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($data as $row)
                      <tr>
                     
                        <td>
                        {{$row->name}}
                        </td>
                        <td>
                        {{$row->email}}
                        </td>
                        <td>
                        {{$row->query}}
                        </td>
                        <td>
                        {{$row->created_at}}
                        </td>
                      </tr>
                     @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        
        </div>
      </div>



      @endsection

