@extends('admin.layouts.master')
@section('title')

Add College
@endsection

@section('content')
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h5 class="title">Add College Details</h5>
              </div>
              <div class="card-body">
              <form action="javascript:;" id="add-form" method="post" enctype="multipart/form-data" >
                @csrf
                <div class="container">
                    <div class="row">
                      <div class="form-group col-12">
                        <label for="uname">Name of University</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="University of California">
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="utype">Category Type</label>
                        <select name="name" class="form-control" id="utype">
                        @foreach($cat as $row) 
                        <option value="{{$row->id}}" class="text-light bg-dark">{{$row->name}}</option>
                         @endforeach
                        </select>
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="utype">Institution Type</label>
                        <select name="type" class="form-control" id="utype">
                          <option  value="Public" class="text-light bg-dark">Public</option>
                          <option  value="Private"class="text-light bg-dark">Private</option>
                        </select>
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="ulevel">Level of Institution</label>
                        <select name="level" class="form-control" id="ulevel">
                          <option class="text-light bg-dark" value="Doctorate">Doctorate</option>
                          <option class="text-light bg-dark" value="Postdoc" >Postdoc</option>
                          <option class="text-light bg-dark" value="Undergraduate" >Undergraduate</option>
                        </select>
                      </div>
                      <div class="form-group col-12 col-md-4">
                        <label for="ucampus">Campus Setting</label>
                        <select class="form-control" name="campus_setting" id="ucampus">
                          <option class="text-light bg-dark" value="Major City">Major City</option>
                          <option class="text-light bg-dark" value="Minor City">Minor City</option>
                        </select>
                      </div>
                      <div class="form-group row ml-5  col-md-6">
                       
                        <div class="form-check col-4 mt-5 ">
                          <input type="checkbox" value="YES" name="SAT" id="sat">
                          <label class="form-check-label" for="sat">
                            SAT
                          </label>
                        </div>
                        <div class="form-check col-4 mt-5 ">
                          <input value="YES" name="ACT"type="checkbox" v id="act" >
                          <label class="form-check-label" for="act">
                            ACT
                          </label>
                        </div>
                        <div class="form-check col-4 mt-5 ">
                          <input value="YES" name="transcript" type="checkbox"  id="transcript" >
                          <label class="form-check-label" for="transcript">
                            Transcript
                          </label>
                        </div>
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="application_type">Application Type</label>
                        <select  name="application_type" class="form-control" id="application_type">
                          <option class="text-light bg-dark">Fall Regular Decision</option>
                          <option class="text-light bg-dark">Spring Regular Decision</option>
                        </select>
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="application_deadline">Application Deadline</label>
                        <input  name="deadline" type="date" class="form-control" id="application_deadline">
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="reply_deadline">Reply Deadline</label>
                        <input name="reply_deadline"type="date" class="form-control" id="reply_deadline">
                      </div>
                      <div class="form-group col-6">
                        <label for="in_state_students">In State Students (Percentage)</label>
                        <input name="in_state_std"type="number" class="form-control" id="in_state_students">
                      </div>
                      <div class="form-group col-6">
                        <label for="out_of_state_students">Out of State Students (Percentage)</label>
                        <input name="out_of_state_std"type="number" class="form-control" id="out_of_state_students">
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="highest_degree">Highest Degree Offered</label>
                        <input name="highest_degree" type="text" class="form-control" id="highest_degree">
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="total_students">Total Number of Students</label>
                        <input name="total_std" type="number" class="form-control" id="total_students">
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="total_undergrads">Total Number of Undergrads</label>
                        <input name="total_ugrad" type="number" class="form-control" id="total_undergrads">
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="in_state_tuition">In-State-Tuition Cost</label>
                        <input name="in_state_tuition"type="number" class="form-control" id="in_state_tuition">
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="out_of_state_tuition">Out_of-State-Tuition Cost</label>
                        <input name="out_of_state_tuition"type="number" class="form-control" id="out_of_state_tuition">
                      </div>
                      <div class="form-group col-12 col-md-4">
                        <label for="average_cost">Average Net Price</label>
                        <input name="avg_net_tuition"type="number" class="form-control" id="average_cost">
                      </div>
                      <div class="form-group col-6 col-md-6">
                        <label for="description">Description</label>
                        <textarea name="description" type="text" class="form-control" id="description">Enter Description here</textarea>
                      </div>
                      <div class="form-group col-6 col-md-6">
                        <label for="address">Address</label>
                        <textarea name="address" type="text" class="form-control" id="address">Enter Address here</textarea>
                      </div>
                      <div class="form-group col-3">
                        <label for="state">State</label>
                        <input name="state"type="text" class="form-control" id="state" >
                      </div>
                      <div class=" col-12 col-md-4">
                        <label for="image">Image</label>
                      
                        <input  id='image' name='image' onChange="document.getElementById('displayimage').src = window.URL.createObjectURL(this.files[0]); " type='file'>
                                          <img style="margin-top:2%;margin-bottom:2%" id="displayimage"  width="30%" src="{{url('assets/mortarboard.svg')}}" alt="...">
                      </div>
                      <!-- <div class="form-group col-12 col-md-4">
                        <label for="status">Status</label>
                        <select name="status" id="status">
                          <option value="1">Active</option>
                          <option value="0">Inactive</option>
                        </select>
                      </div> -->
                    </div>
                  </div>
              </div>
               
             
              <div class="card-footer">
              <div class="alert alert-danger print-error-msg error" style=" display:none">
                                          <ul></ul>
                                       </div>
                                       <div class="alert alert-success print-success-msg success" style="display:none">
                                       <ul></ul>
                                       </div>
                <button type="submit" class="btn btn-fill btn-primary">Save</button>
              </div>
              </form>
            </div>
          </div>
   
        </div>
      </div>

      @endsection

      @section('scripts')
            
<script>
    $(document).ready(function(){

        function printMsg (msg,form) {
            if(form == "error"){
            $(".error").find("ul").html('');
            $('.error').fadeIn("fast", function(){        
            $(".error").delay(5000).fadeOut(4000);
            });
              $.each( msg, function( key, value ) {
                $(".error").find("ul").append('<li>'+value+'</li>');
            });}
            if(form == "success"){
            $(".success").find("ul").html('');
            $('.success').fadeIn("fast", function(){        
            $(".success").delay(5000).fadeOut(4000);
            $(".success").find("ul").append('<li>'+msg+'</li>');
            });
             }
        }
      $('#add-form').submit(function(event) {
            event.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                url:"{{url('add')}}",
                method:"POST",
                data:formData,
                processData: false,
                contentType: false,
                success:function(data)
                { 
                if(data.error){ 
                printMsg(data.error,"error");
                }
                else{
                    printMsg(data.success,"success");
                }
                }
            });    });
    });
</script>
      @endsection