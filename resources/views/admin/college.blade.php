@extends('admin.layouts.master')
@section('title')

Edit College
@endsection

@section('content')
<?php

?>
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h5 class="title">Edit College Details</h5>
              </div>
              <div class="card-body">
              <form action="javascript:;" id="edit-form" method="post" enctype="multipart/form-data" >
                @csrf
                <div class="container">
                    <div class="row">
                      <div class="form-group col-12">
                        <label for="name">Name of University</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{$data->name}}">
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="utype">Category Type</label>
                        <select name="type" class="form-control" id="utype">
                        @foreach($cat as $row) 
                        <?php  if($data->cat_id == $row->id) 
                        $sel = 'selected';
                        else
                        $sel = '';
                        ?>
                        <option value="{{$row->id}}" {{$sel}} class="text-light bg-dark">{{$row->name}}</option>
                         @endforeach
                        </select>
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="utype">Institution Type</label>
                        <select name="type" class="form-control" id="utype">
                        @if($data->type == "Public")

<option  value="Public" class="text-light bg-dark">Public</option>
<option  value="Private"class="text-light bg-dark">Private</option>
@else
<option  value="Public" class="text-light bg-dark">Public</option>
<option  value="Private"class="text-light bg-dark">Private</option>
@endif
                        </select>
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="ulevel">Level of Institution</label>
                        <select name="level" class="form-control" id="ulevel">
                          <option class="text-light bg-dark" value="Doctorate">Doctorate</option>
                          <option class="text-light bg-dark" value="Postdoc" >Postdoc</option>
                          <option class="text-light bg-dark" value="Undergraduate" >Undergraduate</option>
                        </select>
                      </div>
                      <div class="form-group col-12 col-md-4">
                        <label for="ucampus">Campus Setting</label>
                        <select name="level" class="form-control" id="ulevel">
                        @if($data->type == "Doctorate")
                          <option class="text-light bg-dark" value="Doctorate">Doctorate</option>
                          <option class="text-light bg-dark" value="Postdoc" >Postdoc</option>
                          <option class="text-light bg-dark" value="Undergraduate" >Undergraduate</option>
                          @elseif($data->type == "Postdoc")
                          
                          <option class="text-light bg-dark" value="Postdoc" >Postdoc</option>
                          <option class="text-light bg-dark" value="Doctorate">Doctorate</option>
                          <option class="text-light bg-dark" value="Undergraduate" >Undergraduate</option>
                          @else
                          
                          <option class="text-light bg-dark" value="Undergraduate" >Undergraduate</option>
                          <option class="text-light bg-dark" value="Doctorate">Doctorate</option>
                          <option class="text-light bg-dark" value="Postdoc" >Postdoc</option>
                          @endif
                        </select>
                      </div>
                      <div class="form-group row ml-5  col-md-6">
                       <?php  $sat = $act = $tr = "";
                       if($data->SAT == "YES")
                       $sat ="checked";
                       if($data->ACT == "YES")
                       $act ="checked";
                       if($data->transcript == "YES")
                       $tr ="checked";
                       ?>
                        <div class="form-check col-4 mt-5 ">
                          <input {{$sat}} type="checkbox" value="YES" name="SAT" id="sat">
                          <label class="form-check-label" for="sat">
                            SAT
                          </label>
                        </div>
                        <div class="form-check col-4 mt-5 ">
                          <input {{$act}} value="YES" name="ACT"type="checkbox" v id="act" >
                          <label class="form-check-label" for="act">
                            ACT
                          </label>
                        </div>
                        <div class="form-check col-4 mt-5 ">
                          <input value="YES" name="transcript" type="checkbox" {{$tr}} id="transcript" >
                          <label class="form-check-label" for="transcript">
                            Transcript
                          </label>
                        </div>
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="application_type">Application Type</label>
                        <select  name="application_type" class="form-control" id="application_type">
                        <?php  $reg = $spr = $tr = "";
                       if($data->application_type == "Fall Regular Decision")
                       $reg ="selected";
                       if($data->application_type == "Spring Regular Decision")
                       $spr ="selected";
                     
                       ?>
                          <option {{$reg}} class="text-light bg-dark">Fall Regular Decision</option>
                          <option {{$spr}} class="text-light bg-dark">Spring Regular Decision</option>
                        </select>
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="application_deadline">Application Deadline</label>
                        <input  name="deadline" type="date" value="{{$data->deadline}}" class="form-control" id="application_deadline">
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="reply_deadline">Reply Deadline</label>
                        <input name="reply_deadline"type="date" value="{{$data->reply_deadline}}"class="form-control" id="reply_deadline">
                      </div>
                      <div class="form-group col-6">
                        <label for="in_state_students">In State Students (Percentage)</label>
                        <input name="in_state_std"type="number" value="{{$data->in_state_std}}"class="form-control" id="in_state_students">
                      </div>
                      <div class="form-group col-6">
                        <label for="out_of_state_students">Out of State Students (Percentage)</label>
                        <input name="out_of_state_std"type="number" value="{{$data->out_of_state_std}}"class="form-control" id="out_of_state_students">
                      </div>
                      <div class="form-group col-6">
                        <label for="state">State</label>
                        <input name="state"type="text" class="form-control" id="state" value="{{$data->state}}">
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="highest_degree">Highest Degree Offered</label>
                        <input name="highest_degree" type="text" value="{{$data->highest_degree}}" class="form-control" id="highest_degree">
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="total_students">Total Number of Students</label>
                        <input name="total_std" type="number"value="{{$data->total_std}}" class="form-control" id="total_students">
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="total_undergrads">Total Number of Undergrads</label>
                        <input name="total_ugrad" type="number" value="{{$data->total_ugrad}}"class="form-control" id="total_undergrads">
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="in_state_tuition">In-State-Tuition Cost</label>
                        <input name="in_state_tuition"type="number" value="{{$data->in_state_tuition}}"class="form-control" id="in_state_tuition">
                      </div>
                      <div class="form-group col-6 col-md-4">
                        <label for="out_of_state_tuition">Out_of-State-Tuition Cost</label>
                        <input name="out_of_state_tuition"type="number"value="{{$data->out_of_state_tuition}}" class="form-control" id="out_of_state_tuition">
                      </div>
                      <div class="form-group col-12 col-md-4">
                        <label for="average_cost">Average Net Price</label>
                        <input name="avg_net_tuition"type="number" value="{{$data->avg_net_tuition}}"class="form-control" id="average_cost">
                      </div>
                      <div class="form-group col-6 col-md-6">
                        <label for="description">Description</label>
                        <textarea name="description" type="text"value="{{$data->description}}" class="form-control" id="description">{{$data->description}}</textarea>
                      </div>
                      <div class="form-group col-6 col-md-6">
                        <label for="address">Address</label>
                        <textarea name="address" type="text" value="{{$data->address}}" class="form-control" id="address">{{$data->address}}</textarea>
                      </div>
                      <div class=" col-12 col-md-4">
                        <label for="image">Image</label>
                      <input type="hidden" name="id" value="{{$data->id}}">
                        <input  id='image' name='image' onChange="document.getElementById('displayimage').src = window.URL.createObjectURL(this.files[0]); " type='file'>
                                          <img style="margin-top:2%;margin-bottom:2%" id="displayimage"  width="30%" src="{{url('assets/'.$data->image)}}" alt="...">
                      </div>
                      <!-- <div class="form-group col-12 col-md-4">
                        <label for="status">Status</label>
                        <select name="status" id="status">
                          <option value="1">Active</option>
                          <option value="0">Inactive</option>
                        </select>
                      </div> -->
                    </div>
                  </div>
              </div>
               
             
              <div class="card-footer">
              <div class="alert alert-danger print-error-msg error" style=" display:none">
                                          <ul></ul>
                                       </div>
                                       <div class="alert alert-success print-success-msg success" style="display:none">
                                       <ul></ul>
                                       </div>
                <button type="submit" class="btn btn-fill btn-primary">Edit</button>
              </div>
              </form>
            </div>
          </div>
   
        </div>
      </div>

      @endsection

      @section('scripts')
            
<script>
    $(document).ready(function(){

        function printMsg (msg,form) {
            if(form == "error"){
            $(".error").find("ul").html('');
            $('.error').fadeIn("fast", function(){        
            $(".error").delay(5000).fadeOut(4000);
            });
              $.each( msg, function( key, value ) {
                $(".error").find("ul").append('<li>'+value+'</li>');
            });}
            if(form == "success"){
            $(".success").find("ul").html('');
            $('.success').fadeIn("fast", function(){        
            $(".success").delay(5000).fadeOut(4000);
            $(".success").find("ul").append('<li>'+msg+'</li>');
            });
             }
        }
      $('#edit-form').submit(function(event) {
            event.preventDefault();
            var formData = new FormData(this);
            $.ajax({
                url:"{{url('edit')}}",
                method:"POST",
                data:formData,
                processData: false,
                contentType: false,
                success:function(data)
                { 
                if(data.error){ 
                printMsg(data.error,"error");
                }
                else{
                    printMsg(data.success,"success");
                }
                }
            });    });
    });
</script>
      @endsection