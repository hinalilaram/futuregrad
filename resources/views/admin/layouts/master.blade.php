<!--
=========================================================
* * Black Dashboard - v1.0.1
=========================================================

* Product Page: https://www.creative-tim.com/product/black-dashboard
* Copyright 2019 Creative Tim (https://www.creative-tim.com)


* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

@include('admin.includes.head')
<body class="white-content">
    <div class="wrapper">
    @include('admin.includes.sidebar')
      @include('admin.includes.navbar')
         @yield('content')
           @include('admin.includes.footer')
        </div>
    </div>
    @include('admin.includes.scripts')
    @yield('scripts')
</body>

</html>