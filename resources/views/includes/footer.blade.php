
 
      <!-- footer -->
      <section class="bg-primary my-0">
        <div class="container-lg">
          <div class="row text-center text-white justify-content-center align-items-center">
            <div class="col-md-2 mx-2 col-5">
              <p>About Us</p>
              <p>Careers</p>
              <p>Membership</p>
            </div>

            <div class="col-md-2 mx-2 col-5">
              <p>Newsroom</p>
              <p>Research</p>
              <p>College Board Blog</p>
            </div>
    
            <div class="col-md-2 mx-2 col-5">
              <p>Services for students with disabilities</p>
              <p>The Elective</p>
            </div>

            <div class="col-md-2 mx-2 col-5">
              <p>Help</p>
              <p>Contact Us</p>
              <p>More</p>
              
            </div>

          </div>
          <div class="container text-center text-white">
              <i class="bi bi-facebook px-1"></i>
              <i class="bi bi-instagram px-1"></i>
              <i class="bi bi-linkedin px-1"></i>
          </div>
          <hr style="height: 3px; color: white;">
          <p class="text-center text-white">Copyright ©️ 1996-2021 Daniel A. Tysver. All Rights Reserved.</p>
        </div>
      </section>
      
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
    crossorigin="anonymous"></script>
