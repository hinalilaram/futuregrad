<!-- navbar -->
<nav class="navbar navbar-expand-md navbar-dark bg-primary">
        <div class="container-xxl">
            <div class="row py-3">
                <span class="fw-bold text-white navbar-brand fs-6">
                    <img class="img-fluid" src="/assets/mortarboard.svg" alt="logo">
                    fururegrad
                </span>
            </div>
            <!-- toggle button for mobile nav -->
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
            data-bs-target="#main-nav" aria-controls="main-nav" aria-expanded="false"
            aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <!-- navbar links -->
            <div class="collapse navbar-collapse justify-content-end align-center"
            id="main-nav">
                <ul class="navbar-nav">
                    <li class="nav-item px-2">
                        <a class="nav-link text-white" href="{{url('/')}}">Home</a>
                    </li>
                    <li class="nav-item px-2">
                        <a class="nav-link text-white" href="find">Find College</a>
                    </li>
                    <li class="nav-item px-2">
                        <a class="nav-link text-white" href="map">Map</a>
                    </li>
                    <li class="nav-item px-2">
                        <a class="nav-link text-white" href="contact">Contact</a>
                    </li>
                </ul>
            </div>

        </div>
    </nav>