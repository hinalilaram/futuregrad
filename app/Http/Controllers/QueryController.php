<?php

namespace App\Http\Controllers;
use Validator;
use Mail;
use App\Models\Query;
use Illuminate\Http\Request;

class QueryController extends Controller
{
   public function add(Request $request){
    $validator = Validator::make($request->all(), [
        'email' => 'required',
        'query' => 'required',
        'name' => 'required',
    ]);
        if ($validator->passes()) {
            $query = new Query();
            $query->create($request->all());
            $msg = $request->all();

            $data = array('name'=>$request->name, 'email'=>$request->email, 'msg'=>$msg['query']);
   
            Mail::send('mail',['data'=> $data], function($message) use ($data) {
               $message->to('hinalilaram@gmail.com', 'Admin')->subject
                  ('Contact Query');
               $message->from($data['email'] , $data['name']);
            });
            


        return response()->json(['success'=>'Query submitted successfully']);
    }else{
        return response()->json(['error'=>$validator->errors()->all()]);
        }
    
    }

    public function query(){
        $data = Query::all();
                return view('admin.query',['data'=>$data]);
    }
}   
