<?php

namespace App\Http\Controllers;
use App\Models\College;
use App\Models\Category;
use App\Models\Feature;
use App\Models\Review;
use App\Models\Team;
use App\Models\Sitecontent;
use App\Models\Banner;
use Validator;
use Illuminate\Http\Request;

class CollegeController extends Controller
{
    public function home()
    {
        $data['cat']= Category::where('status',1)->get();
        $data['feature']= Feature::where('status',1)->get();
        $data['review']= Review::where('status',1)->get();
        $data['team']= Team::where('status',1)->get();
        $data['banner']= Banner::where('status',1)->get();
        $data['aboutus']= Sitecontent::where('type','AU')->where('status',1)->first();
      //dd($data);
        return view('index',['data'=>$data]);
    }
   public function index(){
      
 
       $data = College::where('status', 1)->get();
       $filtered = [];

       if ($data) {
            $cat = request()->query('cat');
            $search = request()->query('search');
            // $price_min = (int)request()->query('price-min');
            $price_max = (int) request()->query('price-max');
            $majors = request()->query('majors');
            $state = request()->query('state');
            $type = request()->query('type');
            // $price_min = $price_min ? $price_min : 1;
            // $price_max = $price_max ? $price_max : 999999;
// dd($majors );
if($majors){
 
        $data= College::whereIn("cat_id", $majors)->get();
  
}
//dd( $data);
           foreach ($data as $p) {
               $include = true;

               if ($include  && $cat && $cat != $p['cat_id']) {
                   $include = false;
               }
               if ($include && $search && $search != "" && strpos(strtolower($p['name']), $search) === false) {
                $include = false;
               }
               if ($include && $type && $type != $p['type']) {
                   $include = false;
               }
               if ($include && $state && $state != $p['state']) {
                $include = false;
            }
            // if ($include && !($p['avg_net_tuition'] >= $price_min)) {
            //     $include = false;
            // }
            if ($include && !(  $price_max  <= $p['avg_net_tuition'] )) {
                $include = false;
            }


               if ($include) {
                   $filtered[] = $p;
               }
           }
       }

     
      //$filtered;
       return view('find',['data'=>$filtered]);
   }

   public function details(){
    $id = request()->query('id');
    $data = null;
    if($id){
         $data = College::where('id',$id)->where('status',1)->first();
    }
    return view('details',['data'=>$data]);
}

public function list(){
   
         $data = College::where('status',1)->get();
 
    return view('admin.tables',['data'=>$data]);
}
public function show(){
   
    $id = request()->query('id');
    $data = null;
    if($id){
         $data = College::where('id',$id)->where('status',1)->first();
    }
    return view('admin.college',['data'=>$data]);
}
public function addpage(){
    $data = College::where('status',1)->get();
    $cat = Category::where('status',1)->get();
    return view('admin.addcollege',['data'=>$data,'cat'=>$cat]);
}
public function add(Request $request){
    $validator = Validator::make($request->all(), [
        'name' => 'required',
    ]);
        if ($validator->passes()) {
            $college = new College();
            $imgname = null;
            if($request->file("image")){
              
                $img = $request->file("image");
                $imgname = time(). '.' . $img->getClientOriginalExtension() ;
                $img->move(base_path('/assets/'), $imgname);
               
                unset( $request->image);
                }
            
            $college->create($request->all());
            if($imgname !=null){
                $new = College::orderby('id','DESC')->first();
                $new->update(['image'=>$imgname]);
            }
        return response()->json(['success'=>'Collage Added Successfully']);
    }else{
        return response()->json(['error'=>$validator->errors()->all()]);
        }
    }
public function del(){

    $id = request()->query('id');

  
        if ($id) {
            $data = College::where('id',$id)->first();
            if($data){
                $abspath=$_SERVER['DOCUMENT_ROOT'];
                if($data['image'] != null )
                unlink($abspath.'/futuregrad/assets/'.$data->image);
                $data->delete();
            }
        return response()->json(['success'=>'Data Deleted Successfully']);
    }else{
        return response()->json(['error'=>"Something Went Wrong"]);
        }
}
public function editpage(){
    $id = request()->query('id');
    $cat = Category::where('status',1)->get();
    $data = null;
    if($id){
         $data = College::where('id',$id)->where('status',1)->first();
    }
    return view('admin.college',['data'=>$data,'cat'=>$cat]);
}
public function edit(Request $request){
    $validator = Validator::make($request->all(), [
        'id' => 'required',
        'name' => 'required',
    ]);
        if ($validator->passes()) {
            $college = College::where('id',$request->id)->first();
           
            if($request->file("image")){
                
                $abspath=$_SERVER['DOCUMENT_ROOT'];
                if($college['image'] != null )
                unlink($abspath.'/futuregrad/assets/'.$data->image);


                $img = $request->file("image");
                $imgname = time(). '.' . $img->getClientOriginalExtension() ;
                $img->move(base_path('/assets/'), $imgname);
               
                unset( $request->image);
                $college->update(['image'=> $imgname]);
                }
         //   dd($request->name);
            $college->update($request->all());
           
        return response()->json(['success'=>'Collage Updated Successfully']);
    }else{
        return response()->json(['error'=>$validator->errors()->all()]);
        }
}


}
